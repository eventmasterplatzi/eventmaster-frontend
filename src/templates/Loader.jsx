import React from "react";
import "../../assets/styles/Loader.scss";
const Loader = () => (
   <div className="container">
    <p className="title"><span>Event</span>Master</p>
      <div className="lds-grid">
         <div />
         <div />
         <div />
         <div />
         <div />
         <div />
         <div />
         <div />
         <div />
      </div>
   </div>
        
);

export default Loader;
