import React from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import initialState from '../store';
import { createBrowserHistory } from 'history';

const history =createBrowserHistory();
const ProviderMock = props => (

    <Provider store={initialState}>
        <Router history={history}>
            {props.children}
        </Router>
    </Provider>
);

export default ProviderMock;