import React from 'react';
import { mount, shallow } from 'enzyme';
import EventCard from '../../components/EventCard';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<EventCard />', () => {
    test('Render of EventCard component', () => {
        const eventCard = shallow(
            <ProviderMock>
                <EventCard />)
            </ProviderMock>,
            );
        expect(eventCard.length).toEqual(1);
    });
});