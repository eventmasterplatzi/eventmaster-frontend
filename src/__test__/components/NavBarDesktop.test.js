import React from 'react';
import { mount, shallow } from 'enzyme';
import NavBarDesktop from '../../components/NavBarDesktop';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<NavBarDesktop />', () => {
    test('Render of NavBarDesktop component', () => {
        const navBarDesktop = shallow(
            <ProviderMock>
                <NavBarDesktop />)
            </ProviderMock>,
            );
        expect(navBarDesktop.length).toEqual(1);
    });
});