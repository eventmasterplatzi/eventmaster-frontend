import React from 'react';
import { mount, shallow } from 'enzyme';
import CardSponsorMode from '../../components/CardSponsorMode';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<CardSponsorMode />', () => {
    test('Render of CardSponsorMode component', () => {
        const cardSponsorMode = shallow(
            <ProviderMock>
                <CardSponsorMode />)
            </ProviderMock>,
            );
        expect(cardSponsorMode.length).toEqual(1);
    });
});
