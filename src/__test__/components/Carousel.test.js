import React from 'react';
import { mount, shallow } from 'enzyme';
import Carousel from '../../components/Carousel';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<Carousel />', () => {
    test('Render of carousel component', () => {
        const carousel = shallow(
            <ProviderMock>
                <Carousel />)
            </ProviderMock>,
            );
        expect(carousel.length).toEqual(1);
    });
});