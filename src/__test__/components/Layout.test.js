import React from 'react';
import { mount, shallow } from 'enzyme';
import Layout from '../../components/Layout';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<Layout />', () => {
    test('Render of Layout component', () => {
        const layout = shallow(
            <ProviderMock>
                <Layout />)
            </ProviderMock>,
            );
        expect(layout.length).toEqual(1);
    });
});