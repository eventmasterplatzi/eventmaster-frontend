import React from 'react';
import { mount, shallow } from 'enzyme';
import OpenEventCard from '../../components/OpenEventCard';
import ProviderMock from '../../__mocks__/ProviderMock';
import { create } from 'react-test-renderer';

describe('<OpenEventCard />', () => {
    test('Render of OpenEventCard component', () => {
        const openEventCard = shallow(
            <ProviderMock>
                <OpenEventCard />)
            </ProviderMock>,
            );
        expect(openEventCard.length).toEqual(1);
    });
});

describe('<OpenEventCard Snapshot', () => {
    test('Check the OpenEventCard snapshot', () => {
        const openEventCard = create(
            <ProviderMock>
                <OpenEventCard />)
            </ProviderMock>,
            );
            expect(openEventCard.toJSON()).toMatchSnapshot();
    });
});