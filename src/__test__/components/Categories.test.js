import React from 'react';
import { mount, shallow } from 'enzyme';
import Categories from '../../components/Categories';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<Categories />', () => {
    test('Render of Categories component', () => {
        const categories = shallow(
            <ProviderMock>
                <Categories />)
            </ProviderMock>,
            );
        expect(categories.length).toEqual(1);
    });
});