import React from 'react';
import { mount,  } from 'enzyme';
import SearchBar from '../../components/SearchBar';

describe('<SearchBar />', () => {
    test('Render test SearchBar', () => {
        const searchBar = mount(<SearchBar />);
        expect(searchBar.length).toEqual(1);
    });
});