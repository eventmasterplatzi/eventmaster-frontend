import React from 'react';
import { mount, shallow } from 'enzyme';
import ListCategories from '../../components/ListCategories';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<ListCategories />', () => {
    test('Render of ListCategories component', () => {
        const listCategories = shallow(
            <ProviderMock>
                <ListCategories />)
            </ProviderMock>,
            );
        expect(listCategories.length).toEqual(1);
    });
});