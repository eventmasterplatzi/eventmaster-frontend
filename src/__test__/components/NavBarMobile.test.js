import React from 'react';
import { mount, shallow } from 'enzyme';
import NavBarMobile from '../../components/NavbarMobile';
import ProviderMock from '../../__mocks__/ProviderMock';
import { create } from 'react-test-renderer';

describe('<NavBarMobile />', () => {
    test('Render of NavBarMobile component', () => {
        const navBarMobile = shallow(
            <ProviderMock>
                <NavBarMobile />)
            </ProviderMock>,
            );
        expect(navBarMobile.length).toEqual(1);
    });
});

describe('<NavBarMobile Snapshot', () => {
    test('Check the NavBarMobile snapshot', () => {
        const navBarMobile = create(
            <ProviderMock>
                <NavBarMobile />)
            </ProviderMock>,
            );
            expect(navBarMobile.toJSON()).toMatchSnapshot();
    });
});