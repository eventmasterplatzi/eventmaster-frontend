import React from 'react';
import { mount, shallow } from 'enzyme';
import Header from '../../components/Header';
import ProviderMock from '../../__mocks__/ProviderMock';
import { create } from 'react-test-renderer';

describe('<Header />', () => {
    test('Render of header component', () => {
        const header = shallow(
            <ProviderMock>
                <Header />)
            </ProviderMock>,
            );
        expect(header.length).toEqual(1);
    });
    test('Render of the EventMaster logo in the header', () => {
        const header = mount(
            <ProviderMock>
                <Header />)
            </ProviderMock>,
        );
        expect(header.find('.Header__logo').text()).toEqual('EventMaster');
    });
});

describe('<Header Snapshot', () => {
    test('Check the header snapshot', () => {
        const header = create(
            <ProviderMock>
                <Header />)
            </ProviderMock>,
            );
            expect(header.toJSON()).toMatchSnapshot();
    });
});

