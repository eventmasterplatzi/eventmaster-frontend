import React from 'react';
import { mount, shallow } from 'enzyme';
import NewEventForm from '../../components/NewEventForm';
import ProviderMock from '../../__mocks__/ProviderMock';
import { create } from 'react-test-renderer';

describe('<NavBarMobile />', () => {
    test('Render of NavBarMobile component', () => {
        const newEventForm = shallow(
            <ProviderMock>
                <NewEventForm />)
            </ProviderMock>,
            );
        expect(newEventForm.length).toEqual(1);
    });
});
