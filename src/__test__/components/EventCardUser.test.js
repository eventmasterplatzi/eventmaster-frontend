import React from 'react';
import { mount, shallow } from 'enzyme';
import EventCardUser from '../../components/EventCardUser';
import ProviderMock from '../../__mocks__/ProviderMock';

describe('<EventCard />', () => {
    test('Render of EventCardUser component', () => {
        const eventCardUser = shallow(
            <ProviderMock>
                <EventCardUser />)
            </ProviderMock>,
            );
        expect(eventCardUser.length).toEqual(1);
    });
});