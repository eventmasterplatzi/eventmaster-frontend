import React from 'react';
import { mount } from 'enzyme';
import Footer from '../../components/Footer';
import { create } from 'react-test-renderer';

describe('<Footer />', () => {
    const footer = mount(<Footer />);

    test('Render of the footer component', () => {
        expect(footer.length).toEqual(1);
    });
    test('Render of the footer title', () => {
        expect(footer.find('.Footer__terms').text()).toEqual('Terms and Conditions');
    });
});

describe('Footer Snapshot', () => {
    test('Verify UI of footer component', () => {
        const footer = create(<Footer />);
        expect(footer.toJSON()).toMatchSnapshot();
    });
});