import axios from 'axios';
import authHeader from './auth-header';
const API_URL = 'http://eventmaster.site:3001/api/test/';

class UserService {
    getSponsorBoard() {
        return axios.get(API_URL + 'sponsor', { headers: authHeader() });
    }
    getOrganizerBoard() {
        return axios.get(API_URL + 'organizer', {
            headers: authHeader(),
        });
    }
}

export default new UserService();
