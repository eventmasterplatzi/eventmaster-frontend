import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { getCategoriesAction } from "../../actions/categoriesActions";
import { createTopicAction } from "../../actions/topicsActions";
import "../../assets/styles/pages/topics/TopicNew.scss";
import AuthService from "../../services/auth.service";
const TopicNew = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [topic, setTopic] = useState({
    title: "",
    description: "",
    categoryId: "",
  });
  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      const loadCategories = (id) => dispatch(getCategoriesAction(id));
      loadCategories(user.id);
    }
  }, []);
  const categories = useSelector((state) => state.categories.categories);
  const { title, description, categoryId } = topic;
  useEffect(() => {
    if (categories.length) {
      setTopic({ ...topic, categoryId: categories[0].id });
    }
  }, [categories]);
  const addTopic = (topic) => dispatch(createTopicAction(topic));
  const onChange = (e) => {
    setTopic({
      ...topic,
      [e.target.name]: e.target.value,
    });
  };
  const onChangeCategory = (e) => {
    setTopic({
      ...topic,
      categoryId: Number(e.target.value),
    });
  };
  const onSubmit = (e) => {
    e.preventDefault();
    if (title.trim() === "" || description.trim() === "") {
      return;
    }
    console.log(topic);
    addTopic(topic);
    history.push("/organizer");
  };
  return (
    <>
      {/* <main> */}
      <div className="TopicNew">
        <h1 className="TopicNew__title">Add Topic</h1>
        <form className="TopicNew__form" onSubmit={onSubmit}>
          <div className="TopicNew__form-item">
            <label>Name</label>
            <input
              name="title"
              value={title}
              onChange={onChange}
              type="text"
              placeholder="Name"
            />
          </div>
          <div className="TopicNew__form-item">
            <label>Description</label>
            <input
              name="description"
              value={description}
              onChange={onChange}
              type="text"
              placeholder="Description"
            />
          </div>
          <div className="TopicNew__form-item">
            <label>Category</label>
            <select
              className="TopicNew__form-item-select"
              value={categoryId}
              onChange={onChangeCategory}
              name="categoryId"
            >
              {categories.length ? (
                categories.map((category) => (
                  <option key={category.id} value={category.id}>
                    {category.title}
                  </option>
                ))
              ) : (
                <option disabled> There are not categories yet</option>
              )}
            </select>
          </div>
          <button className="TopicNew__form-submit" type="submit">
            Save
          </button>
        </form>
      </div>
      {/* </main> */}
    </>
  );
};

export default TopicNew;
