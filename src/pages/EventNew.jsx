import React from 'react';
import NewEventForm from '../components/NewEventForm';
const EventNew = () => {
    return (
        <>
            <main>
                <NewEventForm />
            </main>
        </>
    );
};

export default EventNew;
