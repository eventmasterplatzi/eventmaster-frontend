import React, { useEffect } from 'react';
import EventList from '../containers/EventList';
import '../assets/styles/pages/PublicHome.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getEventsAction } from '../actions/eventsActions';
const PublicHome = () => {
	const dispatch = useDispatch();
	useEffect(() => {
		const getEvents = () => dispatch(getEventsAction());
		const timeId = setTimeout(() => {
			getEvents();
		}, 100);
		return () => clearTimeout(timeId);
	}, []);
	return (
		<>
			{/* <main> */}
			<div className='PublicHome'>
				<EventList />
			</div>
			{/* </main> */}
		</>
	);
};

export default PublicHome;
