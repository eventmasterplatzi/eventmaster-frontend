import React from 'react';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import AuthService from '../services/auth.service';
import UserProfileForm from '../components/UserProfileForm.jsx';
import axiosClient from '../config/axios';
import Swal from 'sweetalert2';
import '../assets/styles/pages/Profile.scss';

const Profile = () => {
	const history = useHistory();
	const [user, setUser] = useState();
	const [userInfo, setUserInfo] = useState();
	const [image, setImage] = useState({
		userId: '',
		category: '',
	});
	useEffect(() => {
		const currentUser = AuthService.getCurrentUser();
		if (currentUser) {
			userProfile(currentUser.id);
			setUser(currentUser);
		}
	}, []);

	useEffect(() => {
		const currentUser = AuthService.getCurrentUser();
		if (currentUser) {
			userProfile(currentUser.id);
			if (currentUser.roles.includes('ROLE_ORGANIZER')) {
				setImage({
					...image,
					userId: currentUser.id,
					category: 'profile-organizer',
				});
			} else {
				setImage({
					...image,
					userId: currentUser.id,
					category: 'profile-sponsor',
				});
			}
		}
	}, []);

	const userProfile = async (id) => {
		const response = await axiosClient.get(`users/${id}`);
		setUserInfo(response.data);
	};

	const handleChangeImage = (e) => {
		image.image = e.target.files[0];
	};

	const handleSubmitImage = async (e) => {
		e.preventDefault();
		const fd = new FormData();
		fd.append('file', image.image);
		fd.append('userId', image.userId);
		fd.append('category', image.category);
		await axiosClient
			.post('images/upload', fd, {
				onUploadProgress: (progressEvent) => {
					if (
						Math.round(
							(progressEvent.loaded / progressEvent.total) * 100
						) == 100
					) {
						alert('Ready to save');
					}
				},
			})
			.then((res) => {
				image.id = res.data.id;
				const path_image = res.data.path.split('/', 12);
				userInfo.avatar = image.id;
				userInfo.path_image = path_image[path_image.length - 1];
			});
	};

	const handleEditor = (e, editor) => {
		setUserInfo({
			...userInfo,
			biography: editor.getData(),
		});
	};

	const handleChange = (e) => {
		setUserInfo({
			...userInfo,
			[e.target.name]: e.target.value,
		});
	};
	const handleChangeTopics = (e) => {
		setUserInfo({
			...userInfo,
			topics: e.map((value) => value.value),
		});
	};
	const handleChangeCategories = (e) => {
		setUserInfo({
			...userInfo,
			categories: e.map((value) => value.value),
		});
	};

	const userProfileEdit = async (data) => {
		await axiosClient.put(`users/${data.id}`, data);

		console.log(data);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		userProfileEdit(userInfo);
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Your data has been saved',
			showConfirmButton: false,
			timer: 1500,
		});
		history.push('/');
		console.log(userInfo);
	};

	return (
		<main>
			<UserProfileForm
				handleChangeCategories={handleChangeCategories}
				handleChangeTopics={handleChangeTopics}
				handleChange={handleChange}
				handleEditor={handleEditor}
				handleChangeImage={handleChangeImage}
				handleSubmitImage={handleSubmitImage}
				handleSubmit={handleSubmit}
				userInfo={userInfo}
				user={user}
			/>
		</main>
	);
};

export default Profile;
