import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getModelsAction } from "../../actions/modelsActions";
import CardSponsorMode from "../../components/CardSponsorMode";
import AuthService from "../../services/auth.service";
import "../../assets/styles/pages/model/ModelsHome.scss";

const ModelsHome = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      const loadModels = (id) => dispatch(getModelsAction(id));
      loadModels(user.id);
    }
  }, []);
  const models = useSelector((state) => state.models.models);
  return (
    <>
      <main>
        <div className="ModelsHome">
          <div className="ModelsHome__title">My Sponsorship Levels</div>
          <nav className="ModelsHome__create">
            <Link to="/organizer/models/newmodel">
              <button className="btn-newlevel">New Level</button>
            </Link>
          </nav>
          <div className="ModelsHome__models">
            {models.length ? (
              models.map((model) => (
                <CardSponsorMode key={model.id} model={model} />
              ))
            ) : (
              <p className="ModelsHome__models-p">
                You haven't created any sponsorship level yet
              </p>
            )}
          </div>
        </div>
      </main>
    </>
  );
};
export default ModelsHome;
