import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createModelAction } from '../../actions/modelsActions';
import AuthService from '../../services/auth.service';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '../../assets/styles/pages/model/ModelNew.scss';

const ModelNew = ({ history }) => {
	const config = {
		toolbar: [
			'heading',
			'bold',
			'italic',
			'numberedList',
			'bulletedList',
			'blockQuote',
		],
	};
	const [model, setModel] = useState({
		title: '',
		benefits: '',
		ammount: 0,
	});
	const { title, benefits, ammount } = model;
	useEffect(() => {
		const user = AuthService.getCurrentUser();
		if (user) {
			model.userId = user.id;
		}
	}, []);
	const onChange = (e) => {
		setModel({
			...model,
			[e.target.name]: e.target.value,
		});
	};
	const onChangeEditor = (e, editor) => {
		setModel({
			...model,
			benefits: editor.getData(),
		});
	};
	const onChangeAmmount = (e) => {
		setModel({
			...model,
			ammount: Number(e.target.value),
		});
	};
	const dispatch = useDispatch();
	const loading = useSelector((state) => state.models.loading);
	const error = useSelector((state) => state.models.error);
	const addModel = (model) => dispatch(createModelAction(model));

	const onSubmitNewModel = (e) => {
		e.preventDefault();
		if (title.trim() === '' || benefits.trim === '' || ammount <= 0) {
			return;
		}
		addModel(model);
		history.push('/organizer/models');
	};
	return (
		<>
			<main>
				<div className='ModelNew'>
					<h1 className='ModelNew__title'>Add Sponsorship Level</h1>
					<form
						className='ModelNew__form'
						onSubmit={onSubmitNewModel}
					>
						<div className='ModelNew__form-item'>
							<label className='ModelNew__form-header'>
								Name
							</label>
							<input
								className='ModelNew__form-title'
								name='title'
								value={title}
								onChange={onChange}
								type='text'
								placeholder='Name'
							/>
						</div>
						<div className='ModelNew__form-item'>
							<label className='ModelNew__form-header'>
								Ammount
							</label>
							<input
								className='ModelNew__form-ammount'
								name='ammount'
								value={ammount}
								onChange={onChangeAmmount}
								type='number'
							/>
						</div>
						<div className='ModelNew__form-item'>
							<label>Benefits</label>
							<CKEditor
								config={config}
								onChange={onChangeEditor}
								editor={ClassicEditor}
							/>
						</div>
						<button className='ModelNew__form-submit' type='submit'>
							Save
						</button>
					</form>
					{loading ? <p>Loading...</p> : null}
					{error ? <p>There was an error</p> : null}
				</div>
			</main>
		</>
	);
};
export default ModelNew;
