import React, { useState, useEffect } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useDispatch, useSelector } from 'react-redux';
import { editModelAction } from '../../actions/modelsActions';
import { useHistory } from 'react-router-dom';
import '../../assets/styles/pages/model/ModelEdit.scss';

const ModelEdit = () => {
	const config = {
		toolbar: [
			'heading',
			'bold',
			'italic',
			'numberedList',
			'bulletedList',
			'blockQuote',
		],
	};
	const history = useHistory();
	const dispatch = useDispatch();
	const [editModel, setEditModel] = useState({
		title: '',
		benefits: '',
		ammount: 0,
	});
	const model = useSelector((state) => state.models.edit);
	useEffect(() => {
		setEditModel(model);
	}, [model]);
	const { title, benefits, ammount } = editModel;
	const onChange = (e) => {
		setEditModel({
			...editModel,
			[e.target.name]: e.target.value,
		});
	};
	const onChangeEditor = (e, editor) => {
		setEditModel({
			...model,
			benefits: editor.getData(),
		});
	};
	const onChangeAmmount = (e) => {
		setEditModel({
			...editModel,
			ammount: Number(e.target.value),
		});
	};
	const onSubmitEdit = (e) => {
		e.preventDefault();
		dispatch(editModelAction(editModel));
		history.push('/organizer/models');
	};
	return (
		<>
			<main>
				<div className='ModelEdit'>
					<h1 className='ModelNew__title'>Edit Sponsorship Level</h1>

					<form className='ModelEdit__form' onSubmit={onSubmitEdit}>
						<div className='ModelEdit__form-item'>
							<label className='ModelEdit__form-header'>
								Name
							</label>
							<input
								className='ModelEdit__form-title'
								name='title'
								value={title}
								onChange={onChange}
								type='text'
								placeholder='Sponsorship type'
							/>
						</div>
						<div className='ModelEdit__form-item'>
							<label className='ModelEdit__form-header'>
								Ammount
							</label>
							<input
								className='ModelEdit__form-ammount'
								name='ammount'
								value={ammount}
								onChange={onChangeAmmount}
								type='number'
								placeholder='Price'
							/>
						</div>
						<div className='ModelEdit__form-item'>
							<label>Benefits</label>
							<CKEditor
								config={config}
								data={benefits}
								onChange={onChangeEditor}
								editor={ClassicEditor}
							/>
						</div>
						<button
							className='ModelEdit__form-submit'
							type='submit'
						>
							Save
						</button>
					</form>
				</div>
			</main>
		</>
	);
};

export default ModelEdit;
