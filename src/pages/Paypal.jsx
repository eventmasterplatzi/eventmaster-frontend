import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Produc from "../components/Paypal";
import "../assets/styles/pages/Paypal.scss";

const Paypal = () => {
  const models = useSelector((state) => state.models.edit);
  const [model, setBuyModel] = useState();
  useEffect(() => {
    setBuyModel(models);
  }, [models]);
  return (
    <>
      <main className="Paypal">
        <h1 className="Paypal__title">Pay through PayPal</h1>
        {model ? <Produc model={model} /> : null}
      </main>
    </>
  );
};
export default Paypal;
