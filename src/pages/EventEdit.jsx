import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import axiosClient from '../config/axios';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getTopicsAction } from '../actions/topicsActions';
import { getCategoriesAction } from '../actions/categoriesActions';
import { getModelsAction } from '../actions/modelsActions';
import { editEventAction } from '../actions/eventsActions';
import '../assets/styles/components/NewEventForm.scss';
import AuthService from '../services/auth.service';
const EventEdit = () => {
	const config = {
		toolbar: [
			'heading',
			'bold',
			'italic',
			'numberedList',
			'bulletedList',
			'blockQuote',
		],
	};
	const dispatch = useDispatch();
	const history = useHistory();
	const event = useSelector((state) => state.events.edit);
	const [eventEdit, setEventEdit] = useState({
		title: '',
		description: '',
		location: '',
		dateStart: '',
		event_time: '',
		attendees: 0,
		categories: [0],
		topics: [0],
		modes: [0],
	});
	const [editImage, setEditImage] = useState({
		userId: undefined,
		category: '',
	});
	useEffect(() => {
		const user = AuthService.getCurrentUser();
		const loadTopics = () => dispatch(getTopicsAction());
		setEventEdit(event);
		loadTopics();
		if (user) {
			const loadCategories = (id) => dispatch(getCategoriesAction(id));
			const loadModels = (id) => dispatch(getModelsAction(id));
			loadCategories(user.id);
			loadModels(user.id);
			setEditImage({
				...editImage,
				userId: user.id,
				category: 'event',
			});
			eventEdit.userId = user.id;
		}
	}, [event]);
	const loadtopics = useSelector((state) => state.topics.topics);
	const loadcategories = useSelector((state) => state.categories.categories);
	const loadmodels = useSelector((state) => state.models.models);

	if (event) {
		var {
			title,
			location,
			dateStart,
			event_time,
			description,
			attendees,
			modes,
			categories,
			topics,
		} = eventEdit;
	}
	console.log(eventEdit);

	const onChangeImage = (e) => {
		editImage.image = e.target.files[0];
	};
	const onSubmitImage = async (e) => {
		e.preventDefault();
		const fd = new FormData();
		fd.append('file', editImage.image);
		fd.append('userId', editImage.userId);
		fd.append('category', editImage.category);
		await axiosClient.post('images/upload', fd).then((res) => {
			editImage.id = res.data.id;
			eventEdit.image = editImage.id;
		});
	};
	const onChangeEvent = (e) => {
		setEventEdit({
			...eventEdit,
			[e.target.name]: e.target.value,
		});
	};
	const onChangeEditor = (e, editor) => {
		setEventEdit({
			...eventEdit,
			description: editor.getData(),
		});
	};
	const onChangeAttendees = (e) => {
		setEventEdit({
			...eventEdit,
			attendees: Number(e.target.value),
		});
	};
	const onChangeEventSelect = (e) => {
		setEventEdit({
			...eventEdit,
			categories: [e.value],
		});
	};
	const onChangeEventSelectTopics = (e) => {
		setEventEdit({
			...eventEdit,
			topics: e.map((value) => value.value),
		});
		console.log(eventEdit);
	};
	const onChangeEventSelectModes = (e) => {
		setEventEdit({
			...eventEdit,
			modes: e.map((value) => value.value),
		});
	};
	const defaultOptions = (origin, option) => {
		for (let i = 0; i < option.length; i++) {
			if (origin.value == option[i]) {
				return origin.value;
			}
		}
	};
	const topicOptions = loadtopics.map((topic) => {
		return { value: topic.id, label: topic.title };
	});
	const defaultTopicOptions = topicOptions.filter((topic) =>
		defaultOptions(topic, topics)
	);
	const levelOptions = loadmodels.map((model) => {
		return { value: model.id, label: model.title };
	});
	const defaultLevelOptions = levelOptions.filter((model) =>
		defaultOptions(model, modes)
	);
	const category = loadcategories.map((category) => {
		return { value: category.id, label: category.title };
	});
	const defaultCategory = category.filter(
		(category) => category.value == categories.map((category) => category)
	);

	const onSubmitEditEvent = (e) => {
		e.preventDefault();
		dispatch(editEventAction(eventEdit));
		console.log(eventEdit);
		history.push('/organizer');
	};
	return (
		<>
			<div className='NewEventForm'>
				<h1 className='NewEventForm__title'>Edit Event</h1>
				<form
					className='NewEventForm__form'
					onSubmit={onSubmitEditEvent}
				>
					<div className='NewEventForm__form-item img-container'>
						<label>Image</label>
						<input type='file' onChange={onChangeImage} />
						<button onClick={onSubmitImage}>Upload</button>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Title</label>
						<input
							name='title'
							value={title}
							onChange={onChangeEvent}
							type='text'
							placeholder='Title'
						/>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Place</label>
						<input
							name='location'
							value={location}
							onChange={onChangeEvent}
							type='text'
							placeholder='Place'
						/>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Date</label>
						<input
							name='dateStart'
							onChange={onChangeEvent}
							value={dateStart.split('T', 1)}
							type='date'
						/>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Time</label>
						<input
							name='event_time'
							value={event_time}
							onChange={onChangeEvent}
							type='time'
						/>
					</div>
					<div className='NewEventForm__form-item description-newevent'>
						<label>Description</label>
						{description ? (
							<CKEditor
								config={config}
								data={description}
								onChange={onChangeEditor}
								editor={ClassicEditor}
							/>
						) : null}
					</div>
					<div className='NewEventForm__form-item attendees-newevent'>
						<label>Attendees </label>
						<input
							name='attendees'
							value={attendees}
							onChange={onChangeAttendees}
							type='number'
						/>
					</div>
					<div className='NewEventForm__form-item category-newevent'>
						<label>Category </label>
						<Select
							onChange={onChangeEventSelect}
							value={defaultCategory}
							options={category}
						/>
					</div>
					<div className='NewEventForm__form-item topics-newevent'>
						<label>Topics</label>
						<Select
							onChange={onChangeEventSelectTopics}
							value={defaultTopicOptions}
							options={topicOptions}
							isMulti
						/>
					</div>
					<div className='NewEventForm__form-item levels-newevent'>
						<label>Sponsorship Levels</label>
						<Select
							onChange={onChangeEventSelectModes}
							value={defaultLevelOptions}
							options={levelOptions}
							isMulti
						/>
					</div>
					<button className='NewEventForm__form-submit' type='submit'>
						Save
					</button>
				</form>
			</div>
		</>
	);
};

export default EventEdit;
