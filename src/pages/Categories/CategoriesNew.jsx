import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { createCategoryAction } from "../../actions/categoriesActions";
import "../../assets/styles/pages/categories/CategoriesNew.scss";
import AuthService from "../../services/auth.service";

const CategoriesNew = () => {
  const [category, setCategory] = useState({
    title: "",
    description: "",
  });
  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      category.owner = user.id;
    }
  }, []);
  const onChange = (e) => {
    setCategory({
      ...category,
      [e.target.name]: e.target.value,
    });
  };
  const { title, description } = category;
  const dispatch = useDispatch();
  const history = useHistory();
  const addCategory = (category) => dispatch(createCategoryAction(category));
  const onSubmit = (e) => {
    e.preventDefault();
    if (title.trim() === "" || description.trim() === "") {
      return;
    }
    addCategory(category);
    history.push("/organizer");
  };
  return (
    <>
      {/* <main> */}
      <div className="CategoriesNew">
        <h1 className="CategoriesNew__title">Add Category</h1>
        <form className="CategoriesNew__form" onSubmit={onSubmit}>
          <div className="CategoriesNew__form-item">
            <label>Name</label>
            <input
              name="title"
              type="text"
              value={title}
              onChange={onChange}
              placeholder="Name"
            />
          </div>
          <div className="CategoriesNew__form-item">
            <label>Description</label>

            <input
              name="description"
              type="text"
              value={description}
              onChange={onChange}
              placeholder="Description"
            />
          </div>
          <button className="CategoriesNew__form-submit" type="submit">
            Save
          </button>
        </form>
      </div>
      {/* </main> */}
    </>
  );
};

export default CategoriesNew;
