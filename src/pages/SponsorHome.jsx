import React, { useState, useEffect } from 'react';
import EventCardUser from '../components/EventCardUser';
import { getEventsAction } from '../actions/eventsActions';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import AuthService from '../services/auth.service';
import axiosClient from '../config/axios';
import '../assets/font/flaticon.css';
import '../assets/styles/pages/SponsorHome.scss';

const SponsorHome = () => {
	const [userInfo, setUserInfo] = useState();
	const [userEvents, setUserEvents] = useState();
	const [userEventsTwo, setUserEventsTwo] = useState();
	const dispatch = useDispatch();
	const allEvents = useSelector((state) => state.events.events);
	const loading = useSelector((state) => state.events.loading);

	useEffect(() => {
		const getAllEvents = () => dispatch(getEventsAction());
		const currentUser = AuthService.getCurrentUser();
		if (!allEvents.length) {
			getAllEvents();
		}
		if (currentUser) {
			userProfile(currentUser.id);
		}
	}, [allEvents]);
	const defaultOptions = (origin, option) => {
		for (let i = 0; i < option.length; i++) {
			if (origin.categories == option[i]) {
				return origin;
			}
		}
	};
	const defaultOptionsNoRepet = (origin, option) => {
		for (let i = 0; i < option.length; i++) {
			if (origin.id == option[i].id) {
				return null;
			} else {
				return origin;
			}
		}
	};
	const defaultOptionsTopics = (origin, option) => {
		for (let i = 0; i < option.length; i++) {
			for (let i = 0; i < origin.topics.length; i++) {
				if (origin.topics[i] == option[i]) {
					return origin;
				}
			}
		}
	};
	useEffect(() => {
		if (userInfo) {
			const event = allEvents.filter((event) =>
				defaultOptions(event, userInfo.categories)
			);
			const eventTopics = allEvents.filter((event) =>
				defaultOptionsTopics(event, userInfo.topics)
			);

			const noRepit = eventTopics.filter((eventT) =>
				defaultOptionsNoRepet(eventT, event)
			);
			setUserEventsTwo(noRepit);
			setUserEvents(event);
		}
	}, [userInfo]);

	const userProfile = async (id) => {
		const response = await axiosClient.get(`users/${id}`);
		setUserInfo(response.data);
	};

	return (
		<>
			<main>
				<div className='SponsorHome'>
					<section className='SponsorHome__title'>
						<h2>Preferences</h2>
					</section>
					<section className='SponsorHome__events'>
						{loading ? (
							<p>Loading...</p>
						) : userEvents ? (
							userEvents.map((event) => (
								<Link key={event.id} to={`/event/${event.id}`}>
									<EventCardUser
										key={event.id}
										event={event}
										sponsor={true}
									/>
								</Link>
							))
						) : (
							<p>You have no preferences yet</p>
						)}
						{userEventsTwo
							? userEventsTwo.map((event) => (
									<Link
										key={event.id}
										to={`/event/${event.id}`}
									>
										<EventCardUser
											key={event.id}
											event={event}
											sponsor={true}
										/>
									</Link>
							  ))
							: null}
					</section>
				</div>
			</main>
		</>
	);
};

export default SponsorHome;
