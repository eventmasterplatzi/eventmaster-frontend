import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import EventCardUser from '../components/EventCardUser';
import UserService from '../services/user.service';
import { useDispatch, useSelector } from 'react-redux';
import AuthService from '../services/auth.service';
import { getEventsAction } from '../actions/eventsActions';
import '../assets/styles/pages/OrganizerHome.scss';
const OrganizerHome = () => {
	const dispatch = useDispatch();
	const [content, setContent] = useState({});
	const [currentUser, setCurrentUser] = useState({
		userId: undefined,
	});
	const { userId } = currentUser;
	useEffect(() => {
		const user = AuthService.getCurrentUser();
		const getEvents = () => dispatch(getEventsAction());
		getEvents();
		if (user) {
			setCurrentUser({
				userId: user.id,
			});
		}
		UserService.getOrganizerBoard().then((response) => {
			setContent({
				content: response.data,
			});
		});
	}, []);
	useEffect(() => {
		const getEvents = () => dispatch(getEventsAction());
		const timeId = setTimeout(() => {
			getEvents();
		}, 100);
		return () => clearTimeout(timeId);
	}, []);
	const userEvents = useSelector((state) => state.events.events);
	const loading = useSelector((state) => state.events.loading);
	const getUserEvents = userEvents.filter((event) => event.userId === userId);
	return (
		<>
			<div className='organizer'>
				<div className='organizer__nav'>
					<div className='organizer__nav-item navuser-1'>
						<Link to='/organizer/eventnew'>
							<label>Create Event</label>
						</Link>
					</div>
					<div className='organizer__nav-item navuser-2'>
						<Link to='/organizer/models'>
							<label>Sponsorship Levels </label>
						</Link>
					</div>
					<div className='organizer__nav-item navuser-3'>
						<Link to='/organizer/models/newcategory'>
							<label>Add Categories</label>
						</Link>
					</div>
					<div className='organizer__nav-item navuser-4'>
						<Link to='/organizer/models/newtopic'>
							<label>Add Topics</label>
						</Link>
					</div>
				</div>
				<div className='organizer__content'>
					{loading ? (
						<p>Loading...</p>
					) : getUserEvents.length ? (
						getUserEvents.map((event) => (
							<EventCardUser key={event.id} event={event} />
						))
					) : (
						<p>You haven't created any event yet</p>
					)}
				</div>
			</div>
		</>
	);
};

export default OrganizerHome;
