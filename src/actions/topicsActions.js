import axiosClient from '../config/axios';
import {
    CREATE_TOPIC,
    CREATE_TOPIC_SUCCESS,
    CREATE_TOPIC_ERROR,
    DOWNLOADING_TOPICS,
    DOWNLOADING_TOPICS_SUCCESS,
    DOWNLOADING_TOPICS_ERROR,
} from '../reduxTypes/topicsTypes';
import Swal from 'sweetalert2';

//Create categories
export function createTopicAction(topic) {
    return async (dispatch) => {
        dispatch(addTopic());
        try {
            const response = await axiosClient.post('topics', topic);
            dispatch(addTopicSuccess(response.data));
            Swal.fire('Correcto', 'Tu Topico se creo correctamente', 'success');
        } catch (error) {
            dispatch(addTopicError());
        }
    };
}

const addTopic = () => ({
    type: CREATE_TOPIC,
    payload: true,
});

const addTopicSuccess = (topic) => ({
    type: CREATE_TOPIC_SUCCESS,
    payload: topic,
});

const addTopicError = () => ({
    type: CREATE_TOPIC_ERROR,
    payload: true,
});

//Get caegories
export function getTopicsAction() {
    return async (dispatch) => {
        dispatch(getTopics());
        try {
            const response = await axiosClient.get('topics');
            dispatch(getTopicsSuccess(response.data));
        } catch (error) {
            dispatch(getTopicsError());
        }
    };
}
const getTopics = () => ({
    type: DOWNLOADING_TOPICS,
    payload: true,
});

const getTopicsSuccess = (topics) => ({
    type: DOWNLOADING_TOPICS_SUCCESS,
    payload: topics,
});

const getTopicsError = () => ({
    type: DOWNLOADING_TOPICS_ERROR,
    payload: true,
});
