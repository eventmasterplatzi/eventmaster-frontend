import axiosClient from '../config/axios';
import {
	CREATE_CATEGORY,
	CREATE_CATEGORY_SUCCESS,
	CREATE_CATEGORY_ERROR,
	DOWNLOADING_CATEGORIES,
	DOWNLOADING_CATEGORIES_SUCCESS,
	DOWNLOADING_CATEGORIES_ERROR,
} from '../reduxTypes/categoriesTypes';
import Swal from 'sweetalert2';

//Create categories
export function createCategoryAction(category) {
	return async (dispatch) => {
		dispatch(addCategory());
		try {
			const response = await axiosClient.post('categories', category);
			dispatch(addCategorySuccess(response.data));
			Swal.fire(
				'Correcto',
				'Tu categoría se creo correctamente',
				'success'
			);
		} catch (error) {
			dispatch(addCategoryError());
		}
	};
}

const addCategory = () => ({
	type: CREATE_CATEGORY,
	payload: true,
});

const addCategorySuccess = (category) => ({
	type: CREATE_CATEGORY_SUCCESS,
	payload: category,
});

const addCategoryError = () => ({
	type: CREATE_CATEGORY_ERROR,
	payload: true,
});

//Get caegories
export function getCategoriesAction(id) {
	return async (dispatch) => {
		dispatch(getCategories());
		try {
			const response = await axiosClient.get(`categories/user/${id}`);
			dispatch(getCategoriesSuccess(response.data));
		} catch (error) {
			dispatch(getCategoriesError());
		}
	};
}
export function getAllCategoriesAction() {
	return async (dispatch) => {
		dispatch(getCategories());
		try {
			const response = await axiosClient.get(`categories`);
			dispatch(getCategoriesSuccess(response.data));
		} catch (error) {
			dispatch(getCategoriesError());
		}
	};
}
const getCategories = () => ({
	type: DOWNLOADING_CATEGORIES,
	payload: true,
});

const getCategoriesSuccess = (categories) => ({
	type: DOWNLOADING_CATEGORIES_SUCCESS,
	payload: categories,
});

const getCategoriesError = () => ({
	type: DOWNLOADING_CATEGORIES_ERROR,
	payload: true,
});
