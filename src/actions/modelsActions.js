import axiosClient from '../config/axios';
import {
    CREATE_MODEL,
    CREATE_MODEL_SUCCESS,
    CREATE_MODEL_ERROR,
    DOWNLOADING_MODELS,
    DOWNLOADING_MODELS_SUCCESS,
    DOWNLOADING_MODELS_ERROR,
    DELETE_MODELS,
    DELETE_MODELS_SUCCESS,
    DELETE_MODELS_ERROR,
    EDIT_MODELS,
    EDIT_MODELS_START,
    EDIT_MODELS_SUCCESS,
    EDIT_MODELS_ERROR,
} from '../reduxTypes/modelsTypes';
import Swal from 'sweetalert2';

//Create models
export function createModelAction(model) {
    return async (dispatch) => {
        dispatch(addModel());
        try {
            const response = await axiosClient.post('modes', model);
            dispatch(addModelSuccess(response.data));
            Swal.fire('Correcto', 'Tu modelo se creo correctamente', 'success');
        } catch (error) {
            dispatch(addModelError());
        }
    };
}

const addModel = () => ({
    type: CREATE_MODEL,
    payload: true,
});

const addModelSuccess = (model) => ({
    type: CREATE_MODEL_SUCCESS,
    payload: model,
});

const addModelError = () => ({
    type: CREATE_MODEL_ERROR,
    payload: true,
});

//Get models per user
export function getModelsAction(id) {
    return async (dispatch) => {
        dispatch(getModels());
        try {
            const response = await axiosClient.get(`modes/user?userId=${id}`);
            dispatch(getUserModelsSuccess(response.data));
        } catch (error) {
            dispatch(getUserModelsError());
        }
    };
}

const getModels = () => ({
    type: DOWNLOADING_MODELS,
    payload: true,
});

const getUserModelsSuccess = (models) => ({
    type: DOWNLOADING_MODELS_SUCCESS,
    payload: models,
});

const getUserModelsError = () => ({
    type: DOWNLOADING_MODELS_ERROR,
    payload: true,
});

//Delete models

export function deleteModelAction(id) {
    return async (dispatch) => {
        dispatch(deleteModel(id));
        try {
            await axiosClient.delete(`modes/${id}`);
            dispatch(deleteModelSucces());
        } catch (error) {
            dispatch(deleteModelError());
        }
    };
}

const deleteModel = (id) => ({
    type: DELETE_MODELS,
    payload: id,
});

const deleteModelSucces = () => ({
    type: DELETE_MODELS_SUCCESS,
});

const deleteModelError = () => ({
    type: DELETE_MODELS_ERROR,
    payload: true,
});

//edit models

export function getModelEdit(model) {
    return (dispatch) => {
        dispatch(getModelEditAction(model));
    };
}

const getModelEditAction = (model) => ({
    type: EDIT_MODELS,
    payload: model,
});

//Edit on the server and state

export function editModelAction(model) {
    return async (dispatch) => {
        dispatch(editModel());
        try {
            await axiosClient.put(`modes/${model.id}`, model);
            dispatch(editModelSuccess(model));
        } catch (error) {
            dispatch(editModelError());
        }
    };
}

const editModel = () => ({
    type: EDIT_MODELS_START,
});

const editModelSuccess = (model) => ({
    type: EDIT_MODELS_SUCCESS,
    payload: model,
});

const editModelError = () => ({
    type: EDIT_MODELS_ERROR,
    payload: true,
});
