import axiosClient from '../config/axios';
import {
	CREATE_EVENT,
	CREATE_EVENT_ERROR,
	CREATE_EVENT_SUCCESS,
	GET_EVENT,
	GET_EVENT_ERROR,
	GET_EVENT_SUCCESS,
	DELETE_EVENT,
	DELETE_EVENT_SUCCESS,
	DELETE_EVENT_ERROR,
	EDIT_EVENT,
	EDIT_EVENT_START,
	EDIT_EVENT_SUCCESS,
	EDIT_EVENT_ERROR,
} from '../reduxTypes/eventsTypes';
import Swal from 'sweetalert2';
export function createEventAction(event) {
	return async (dispatch) => {
		dispatch(addEvent());
		try {
			const response = await axiosClient.post('events', event);
			console.log('Secreo el evento: ', response.data);
			dispatch(addEventSuccess(response.data));
			Swal.fire('Correcto', 'Tu evento se creo correctamente', 'success');
		} catch (error) {
			dispatch(addEventError());
		}
	};
}

const addEvent = () => ({
	type: CREATE_EVENT,
	payload: true,
});

const addEventSuccess = (event) => ({
	type: CREATE_EVENT_SUCCESS,
	payload: event,
});

const addEventError = () => ({
	type: CREATE_EVENT_ERROR,
	payload: true,
});

export function getEventsAction() {
	return async (dispatch) => {
		dispatch(getEvents());
		try {
			let events = [];
			const response = await axiosClient.get('events');
			const ids = response.data.map((id) => id.id);

			for (let i = 0; i < ids.length; i++) {
				const responseEvents = await axiosClient.get(
					`events/${ids[i]}`
				);
				events.push(responseEvents.data);
			}

			dispatch(getEventsSuccess(events));
		} catch (error) {
			dispatch(getEventsError());
		}
	};
}

const getEvents = () => ({
	type: GET_EVENT,
	payload: true,
});

const getEventsSuccess = (events) => ({
	type: GET_EVENT_SUCCESS,
	payload: events,
});

const getEventsError = () => ({
	type: GET_EVENT_ERROR,
	payload: true,
});

export function deleteEventAction(id) {
	return async (dispatch) => {
		dispatch(deleteEvent(id));
		try {
			const response = await axiosClient.delete(`events/${id}`);
			dispatch(deleteEventSuccess());
			Swal.fire('Deleted!', response.data.message, 'success');
		} catch (err) {
			dispatch(deleteEventError());
		}
	};
}

const deleteEvent = (id) => ({
	type: DELETE_EVENT,
	payload: id,
});

const deleteEventSuccess = () => ({
	type: DELETE_EVENT_SUCCESS,
});

const deleteEventError = () => ({
	type: DELETE_EVENT_ERROR,
	payload: true,
});

export function getEventEditAction(event) {
	return (dispatch) => {
		dispatch(getEventEdit(event));
	};
}

const getEventEdit = (event) => ({
	type: EDIT_EVENT,
	payload: event,
});

export function editEventAction(event) {
	return async (dispatch) => {
		dispatch(editEvent());
		try {
			await axiosClient.put(`events/${event.id}`, event);
			Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Your event has been updated',
				showConfirmButton: true,
			});
			dispatch(editEventSuccess(event));
		} catch (error) {
			dispatch(editEventError());
		}
	};
}

const editEvent = () => ({
	type: EDIT_EVENT_START,
});

const editEventSuccess = (event) => ({
	type: EDIT_EVENT_SUCCESS,
	payload: event,
});

const editEventError = () => ({
	type: EDIT_EVENT_ERROR,
	payload: true,
});
