import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Paypal from '../pages/Paypal';
import Profile from '../pages/Profile';
import ModelNew from '../pages/Model/ModelNew';
import TopicNew from '../pages/Topics/TopicNew';
import EventNew from '../pages/EventNew';
import ModelEdit from '../pages/Model/ModelEdit';
import EventEdit from '../pages/EventEdit';
import PublicHome from '../pages/PublicHome';
import ModelsHome from '../pages/Model/ModelsHome';
import SponsorHome from '../pages/SponsorHome';
import CategoriesNew from '../pages/Categories/CategoriesNew';
import OrganizerHome from '../pages/OrganizerHome';
import Layout from '../components/Layout';
import OpenEventCard from '../components/OpenEventCard';
import Login from '../containers/Login';
import Register from '../containers/Register';
const App = () => (
	<Router>
		<Layout>
			<Switch>
				<Route exact path='/' component={PublicHome} />
				<Route exact path='/sponsor' component={SponsorHome} />
				<Route exact path='/organizer' component={OrganizerHome} />
				<Route exact path='/organizer/models' component={ModelsHome} />
				<Route
					exact
					path='/organizer/models/newmodel'
					component={ModelNew}
				/>
				<Route
					exact
					path='/organizer/models/editmodel/:id'
					component={ModelEdit}
				/>
				<Route
					exact
					path='/organizer/models/newcategory'
					component={CategoriesNew}
				/>
				<Route
					exact
					path='/organizer/models/newtopic'
					component={TopicNew}
				/>
				<Route exact path='/organizer/eventnew/' component={EventNew} />
				<Route
					exact
					path='/organizer/eventedit/:id'
					component={EventEdit}
				/>
				<Route exact path='/login' component={Login} />
				<Route exact path='/register' component={Register} />
				<Route exact path='/profile' component={Profile} />
				<Route exact path='/event/:eventId' component={OpenEventCard} />
				<Route exact path='/buy/:id' component={Paypal} />
			</Switch>
		</Layout>
	</Router>
);

export default App;
