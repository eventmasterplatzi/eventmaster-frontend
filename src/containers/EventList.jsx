import React, { useEffect } from 'react';
import EventCard from '../components/EventCard';
import SearchBar from '../components/SearchBar';
import { useDispatch, useSelector } from 'react-redux';
import { getEventsAction } from '../actions/eventsActions';
const EventList = () => {
	const dispatch = useDispatch();
	const events = useSelector((state) => state.events.events);
	const loading = useSelector((state) => state.events.loading);
	useEffect(() => {
		const getEvents = () => dispatch(getEventsAction());
		if (!events.length) {
			getEvents();
		}
	}, []);

	return (
		<>
			<section className='PublicHome__searchBar'>
				<h1 className='PublicHome__searchBar-title'>
					Discover new events
				</h1>
				<SearchBar />
			</section>
			<section className='PublicHome__carousel'>
				{loading ? (
					<p>Loading...</p>
				) : events.length ? (
					events.map((event) => (
						<EventCard key={event.id} event={event} />
					))
				) : (
					<p>There are no events to show</p>
				)}
			</section>
		</>
	);
};

export default EventList;
