import React, { useState } from 'react';
import AuthService from '../services/auth.service';
import { useHistory } from 'react-router-dom';
import '../assets/styles/containers/Login.scss';
import Swal from 'sweetalert2';
const Login = () => {
	const history = useHistory();
	const [form, setValues] = useState({
		username: '',
		password: '',
		message: '',
	});
	const { message } = form;
	const { username, password } = form;
	const handleChange = (event) => {
		setValues({
			...form,
			[event.target.name]: event.target.value,
		});
	};
	const handleSubmit = (event) => {
		event.preventDefault();
		AuthService.login(username, password)
			.then(() => {
				console.log('Entro');
				history.push('/');
			})
			.catch((error) => {
				setValues({ ...form, message: error.response.data.message });
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Something went wrong!',
				});
			});
	};

	return (
		<>
			<div className='Login'>
				<form className='Login__form' onSubmit={handleSubmit}>
					<input
						className='Login__form-username'
						onChange={handleChange}
						type='text'
						name='username'
						placeholder='Username'
					/>
					<input
						className='Login__form-password'
						onChange={handleChange}
						type='password'
						name='password'
						placeholder='Password'
					/>
					<button className='Login__form-button' type='submit'>
						Log In
					</button>
					<a>I forgot my password</a>
				</form>
			</div>
		</>
	);
};

export default Login;
