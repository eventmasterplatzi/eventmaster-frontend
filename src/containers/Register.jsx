import React, { useState } from 'react';
import AuthService from '../services/auth.service';
import { useHistory } from 'react-router-dom';
import '../assets/styles/containers/Register.scss';

const Register = () => {
	const history = useHistory();
	const [form, setValues] = useState({
		name: '',
		username: '',
		email: '',
		password: '',
		roles: ['sponsor'],
		message: '',
	});
	const { name, username, email, password, roles, message } = form;
	const handleName = (event) => {
		setValues({
			...form,
			name: event.target.value,
		});
	};
	const handleEmail = (event) => {
		setValues({
			...form,
			email: event.target.value,
		});
	};
	const handleUsername = (event) => {
		setValues({
			...form,
			username: event.target.value,
		});
	};
	const handlePassword = (event) => {
		setValues({
			...form,
			password: event.target.value,
		});
	};
	const handleRole = (event) => {
		setValues({
			...form,
			roles: [event.target.value],
		});
	};
	const handleSubmit = (event) => {
		event.preventDefault();
		AuthService.register(name, username, email, password, roles)
			.then(() => {
				history.push('/login');
			})
			.catch((error) => {
				setValues({ message: error.response.data.message });
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: message,
				});
			});
	};
	return (
		<section className='Register'>
			<form className='Register__form' onSubmit={handleSubmit}>
				<input
					onChange={handleName}
					name='name'
					type='text'
					placeholder='Full name'
				/>
				<input
					onChange={handleUsername}
					name='username'
					type='text'
					placeholder='Nickname'
				/>
				<input
					onChange={handleEmail}
					name='email'
					type='email'
					placeholder='E-mail'
				/>
				<input
					onChange={handlePassword}
					name='password'
					type='password'
					placeholder='Password'
				/>
				<p>Select your role</p>
				<div className='Register__form-role'>
					<select
						className='Register__form-role-select'
						name='roles'
						onChange={handleRole}
					>
						<option>sponsor</option>
						<option>organizer</option>
					</select>
				</div>
				<button type='submit'>Register</button>
			</form>
		</section>
	);
};

export default Register;
