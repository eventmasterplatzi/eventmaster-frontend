import React from "react";
import { Link } from "react-router-dom";
import EventCardUser from "../components/EventCardUser";
import post from "../assets/images/post-1.png";
import post2 from "../assets/images/post-2.png";
import post3 from "../assets/images/post-3.png";
const eventData = [
  {
    id: 1,
    title: "PlatziConf",
    image: post,
    description: "Una buena descripcion",
    place: "Bogotá",
    date: "30-05-2020",
  },
  {
    id: 2,
    title: "IronHack",
    image: post2,
    description: "Una buena descripcion",
    place: "Medellín",
    date: "05-06-2020",
  },
  {
    id: 3,
    title: "Dev.f",
    image: post3,
    description: "Una buena descripcion",
    place: "Bogotá",
    date: "15-06-2020",
  },
  {
    id: 4,
    title: "PlatziConf",
    image: post,
    description: "Una buena descripcion",
    place: "Bogotá",
    date: "30-05-2020",
  },
  {
    id: 5,
    title: "IronHack",
    image: post2,
    description: "Una buena descripcion",
    place: "Medellín",
    date: "05-06-2020",
  },
  {
    id: 6,
    title: "Dev.f",
    image: post3,
    description: "Una buena descripcion",
    place: "Bogotá",
    date: "15-06-2020",
  },
];
const EventListUser = () => {
  return eventData.map((event) => (
    <Link to={`/event/${event.id}`}>
      <EventCardUser key={event.id} {...event} />
    </Link>
  ));
};

export default EventListUser;
