import {
    CREATE_MODEL,
    CREATE_MODEL_SUCCESS,
    CREATE_MODEL_ERROR,
    DOWNLOADING_MODELS,
    DOWNLOADING_MODELS_SUCCESS,
    DOWNLOADING_MODELS_ERROR,
    DELETE_MODELS,
    DELETE_MODELS_SUCCESS,
    DELETE_MODELS_ERROR,
    EDIT_MODELS,
    EDIT_MODELS_SUCCESS,
    EDIT_MODELS_ERROR,
} from '../reduxTypes/modelsTypes';

const INITIAL_STATE = {
    models: [],
    error: null,
    loading: false,
    delete: null,
    edit: null,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case CREATE_MODEL:
        case DOWNLOADING_MODELS:
            return { ...state, loading: action.payload };
        case CREATE_MODEL_SUCCESS:
            return {
                ...state,
                loading: false,
                models: [...state.models, action.payload],
            };
        case CREATE_MODEL_ERROR:
        case DOWNLOADING_MODELS_ERROR:
        case DELETE_MODELS_ERROR:
            return { ...state, loading: false, error: action.payload };
        case DOWNLOADING_MODELS_SUCCESS:
            return { ...state, loading: false, models: action.payload };
        case DELETE_MODELS:
            return { ...state, delete: action.payload };
        case DELETE_MODELS_SUCCESS:
            return {
                ...state,
                models: state.models.filter(
                    (model) => model.id !== state.delete
                ),
                delete: null,
            };
        case EDIT_MODELS:
            return { ...state, edit: action.payload };
        case EDIT_MODELS_SUCCESS:
            return {
                ...state,
                edit: null,
                models: state.models.map((model) =>
                    model.id == action.payload.id
                        ? (model = action.payload)
                        : model
                ),
            };
        default:
            return state;
    }
}
