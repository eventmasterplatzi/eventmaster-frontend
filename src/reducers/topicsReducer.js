import {
    CREATE_TOPIC,
    CREATE_TOPIC_SUCCESS,
    CREATE_TOPIC_ERROR,
    DOWNLOADING_TOPICS,
    DOWNLOADING_TOPICS_SUCCESS,
    DOWNLOADING_TOPICS_ERROR,
} from '../reduxTypes/topicsTypes';

const INITIAL_STATE = {
    topics: [],
    error: null,
    loading: false,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case CREATE_TOPIC:
        case DOWNLOADING_TOPICS:
            return { ...state, loading: action.payload };
        case CREATE_TOPIC_SUCCESS:
            return {
                ...state,
                loading: false,
                topics: [...state.topics, action.payload],
            };

        case CREATE_TOPIC_ERROR:
        case DOWNLOADING_TOPICS_ERROR:
            return { ...state, loading: false, error: action.payload };
        case DOWNLOADING_TOPICS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: false,
                topics: action.payload,
            };
        default:
            return state;
    }
}
