import {
	CREATE_EVENT,
	CREATE_EVENT_ERROR,
	CREATE_EVENT_SUCCESS,
	GET_EVENT,
	GET_EVENT_ERROR,
	GET_EVENT_SUCCESS,
	DELETE_EVENT,
	DELETE_EVENT_SUCCESS,
	DELETE_EVENT_ERROR,
	EDIT_EVENT,
	EDIT_EVENT_SUCCESS,
	EDIT_EVENT_ERROR,
} from '../reduxTypes/eventsTypes';

const INITIAL_STATE = {
	events: [],
	error: null,
	loading: false,
	delete: null,
	edit: null,
};

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case CREATE_EVENT:
		case GET_EVENT:
		case EDIT_EVENT_ERROR:
			return { ...state, loading: action.payload };
		case CREATE_EVENT_SUCCESS:
			return {
				...state,
				loading: false,
				events: [...state.events, action.payload],
			};
		case CREATE_EVENT_ERROR:
		case GET_EVENT_ERROR:
		case DELETE_EVENT_ERROR:
			return { ...state, loading: false, error: action.payload };
		case GET_EVENT_SUCCESS:
			return { ...state, loading: false, events: action.payload };
		case DELETE_EVENT:
			return { ...state, delete: action.payload };
		case DELETE_EVENT_SUCCESS:
			return {
				...state,
				events: state.events.filter(
					(event) => event.id !== state.delete
				),
				delete: null,
			};
		case EDIT_EVENT:
			return { ...state, edit: action.payload };
		case EDIT_EVENT_SUCCESS:
			return {
				...state,
				edit: null,
				events: state.events.map((event) =>
					event.id === action.payload.id
						? (event = action.payload)
						: event
				),
			};
		default:
			return state;
	}
}
