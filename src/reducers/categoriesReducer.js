import {
    CREATE_CATEGORY,
    CREATE_CATEGORY_SUCCESS,
    CREATE_CATEGORY_ERROR,
    DOWNLOADING_CATEGORIES,
    DOWNLOADING_CATEGORIES_SUCCESS,
    DOWNLOADING_CATEGORIES_ERROR,
} from '../reduxTypes/categoriesTypes';

const INITIAL_STATE = {
    categories: [],
    error: null,
    loading: false,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case CREATE_CATEGORY:
        case DOWNLOADING_CATEGORIES:
            return { ...state, loading: action.payload };
        case CREATE_CATEGORY_SUCCESS:
            return {
                ...state,
                loading: false,
                categories: [...state.categories, action.payload],
            };

        case CREATE_CATEGORY_ERROR:
        case DOWNLOADING_CATEGORIES_ERROR:
            return { ...state, loading: false, error: action.payload };
        case DOWNLOADING_CATEGORIES_SUCCESS:
            return {
                ...state,
                loading: false,
                error: false,
                categories: action.payload,
            };
        default:
            return state;
    }
}
