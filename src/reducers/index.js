import { combineReducers } from 'redux';
import modelsReducer from './modelsReducer';
import eventsReducer from './eventsReducer';
import categoriesReducer from './categoriesReducer';
import topicsReducer from './topicsReducer';
export default combineReducers({
    models: modelsReducer,
    categories: categoriesReducer,
    topics: topicsReducer,
    events: eventsReducer,
});
