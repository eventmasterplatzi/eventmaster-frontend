import React from "react";
import "../assets/styles/components/Footer.scss";

const Footer = () => {
  return (
    <>
      <footer className="Footer">
        <a className="Footer__terms" href="">
          Terms and Conditions
        </a>
      </footer>
    </>
  );
};

export default Footer;
