import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { deleteModelAction, getModelEdit } from '../actions/modelsActions';
import '../assets/styles/components/CardSponsorMode.scss';

const CardSponsorMode = ({ model, buy }) => {
	const { title, benefits, ammount, id } = model;
	const dispatch = useDispatch();
	const history = useHistory();
	const deleteConfirm = (id) => {
		dispatch(deleteModelAction(id));
	};
	const redirectEdit = (model) => {
		dispatch(getModelEdit(model));
		history.push(`/organizer/models/editmodel/${model.id}`);
	};
	const redirectPaypal = (model) => {
		dispatch(getModelEdit(model));
		history.push(`/buy/${model.id}`);
	};
	return (
		<>
			<div className='sponsor-modes'>
				<div className='sponsor-modes__type'>{title}</div>
				<div className='sponsor-modes__price'>${ammount}</div>
				<div className='sponsor-modes__frequency'>Per sponsor</div>
				<div className='sponsor-modes__description'>
					{/* <p className='sponsor-modes__description-text'> */}
					{ReactHtmlParser(benefits)}
					{/* </p> */}
				</div>
				{!buy ? (
					<div className='sponsor-modes__select'>
						<button
							type='button'
							onClick={() => deleteConfirm(id)}
							className='sponsor-modes__select-delete'
						>
							Delete
						</button>

						<button
							type='button'
							onClick={() => redirectEdit(model)}
							className='sponsor-modes__select-edit'
						>
							Edit
						</button>
					</div>
				) : (
					<div className='sponsor-modes__select'>
						<button
							type='button'
							onClick={() => redirectPaypal(model)}
							className='sponsor-modes__select-edit'
						>
							Buy
						</button>
					</div>
				)}
			</div>
		</>
	);
};

export default CardSponsorMode;
