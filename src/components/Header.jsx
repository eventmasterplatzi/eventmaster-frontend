import React from 'react';
import { Link } from 'react-router-dom';

import '../assets/styles/components/Header.scss';
import {
    AiOutlineSearch,
    AiOutlineBell,
    AiOutlineMessage,
    AiOutlineUser,
} from 'react-icons/ai';

const Header = () => {
    return (
        <header>
            <div className='Header'>
                <div className='Header__title'>
                    <Link to='/'>
                        <h1 className='Header__logo'>
                            <strong>Event</strong>Master
                        </h1>
                    </Link>
                </div>
                <div className='Header__options'>
                    <div className='Header__options-item'>
                        <div className='Header__options-item-icon'>
                            <AiOutlineSearch />
                        </div>
                        <div className='Header__options-item-text'>Search</div>
                    </div>
                    <div className='Header__options-item'>
                        <div className='Header__options-item-icon'>
                            <AiOutlineBell />
                        </div>
                        <div className='Header__options-item-text'>Alerts</div>
                    </div>
                    <div className='Header__options-item'>
                        <div className='Header__options-item-icon'>
                            <AiOutlineMessage />
                        </div>
                        <div className='Header__options-item-text'>Chat</div>
                    </div>
                    <div className='Header__options-item'>
                        <div className='Header__options-item-icon'>
                            <AiOutlineUser />
                        </div>
                        <div className='Header__options-item-text'>User</div>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
