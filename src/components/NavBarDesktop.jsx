import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import AuthService from "../services/auth.service";
import "../assets/styles/components/NavbarDesktop.scss";

const NavBarDesktop = () => {
  const history = useHistory();
  const [user, setUser] = useState({
    showOrganizerBoard: false,
    showSponsorBoard: false,
    currentUser: undefined,
  });
  const handleLogout = () => {
    AuthService.logout();
    history.push("/login");
  };
  useEffect(() => {
    var user = AuthService.getCurrentUser();
    if (user) {
      setUser({
        currentUser: user,
        showOrganizerBoard: user.roles.includes("ROLE_ORGANIZER"),
        showSponsorBoard: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }, []);
  const { currentUser, showOrganizerBoard, showSponsorBoard } = user;
  return (
    <>
      <header>
        <div className="Header">
          <div className="Header__title">
            <Link to="/">
              <h1 className="Header__logo">
                <strong>Event</strong>Master
              </h1>
            </Link>
          </div>
          {currentUser ? (
            <div className="Header__options">
              <Link to={showOrganizerBoard ? "/organizer" : "/sponsor"}>
                <div className="Header__options-item">
                  <div className="Header__options-item-text">Dashboard</div>
                </div>
              </Link>
              <Link to="/">
                <div className="Header__options-item">
                  <div className="Header__options-item-text">Search</div>
                </div>
              </Link>
              {/* <Link to="/">
                <div className="Header__options-item">
                  <div className="Header__options-item-text">Alerts</div>
                </div>
              </Link> */}
              <Link to="/profile">
                <div className="Header__options-item">
                  <div className="Header__options-item-text">Profile</div>
                </div>
              </Link>
              <div className="Header__options-item">
                {currentUser ? (
                  <Link to="/" onClick={handleLogout}>
                    <div className="Header__options-item-text">Log Out</div>
                  </Link>
                ) : (
                  <div className="Header__options-item-text">User</div>
                )}
              </div>
            </div>
          ) : (
            <div className="Header__options">
              <div className="Header__options-item">
                <Link to="/login">
                  <div className="Header__options-item-text">Log In</div>
                </Link>
                <Link to="/register">
                  <div className="Header__options-item-text">Register</div>
                </Link>
              </div>
            </div>
          )}
        </div>
      </header>
    </>
  );
};

export default NavBarDesktop;
