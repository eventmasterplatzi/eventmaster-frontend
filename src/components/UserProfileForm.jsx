import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Select from 'react-select';
import { getAllCategoriesAction } from '../actions/categoriesActions';
import { getTopicsAction } from '../actions/topicsActions';
import '../assets/styles/components/UserProfileForm.scss';
const API = 'http://eventmaster.site/eventmaster-backend/resources/assets';
const UserProfileForm = ({
	handleChangeCategories,
	handleChangeTopics,
	handleChangeImage,
	handleSubmitImage,
	handleEditor,
	handleChange,
	handleSubmit,
	userInfo,
	user,
}) => {
	if (userInfo) {
		var {
			email,
			username,
			biography,
			phone,
			paypal_account,
			avatar,
			topics,
			categories,
			id,
		} = userInfo;
		if (!biography) biography = '';
	}
	if (user) {
		var category_image = '';
		if (user.roles.includes('ROLE_SPONSOR')) {
			category_image = 'profile-sponsor';
		} else {
			category_image = 'profile-organizer';
		}
	}

	const config = {
		toolbar: [
			'heading',
			'bold',
			'italic',
			'numberedList',
			'bulletedList',
			'|',
			'blockQuote',
		],
	};
	const dispatch = useDispatch();
	const loadcategories = useSelector((state) => state.categories.categories);
	const loadtopics = useSelector((state) => state.topics.topics);

	useEffect(() => {
		const getAllCategories = () => dispatch(getAllCategoriesAction());
		const getAllTopics = () => dispatch(getTopicsAction());
		if (user) {
			if (user.roles.includes('ROLE_SPONSOR')) {
				if (!loadcategories.length) {
					getAllCategories();
				}
				if (!loadtopics.length) {
					getAllTopics();
				}
			}
		}
	}, [user]);

	const defaultOptions = (origin, option) => {
		for (let i = 0; i < option.length; i++) {
			if (origin.value == option[i]) {
				return origin.value;
			}
		}
	};

	const category = loadcategories.map((category) => {
		return { value: category.id, label: category.title };
	});
	if (categories) {
		var categoriesUser = category.filter((category) =>
			defaultOptions(category, categories)
		);
	}
	const topicOptions = loadtopics.map((topic) => {
		return { value: topic.id, label: topic.title };
	});
	if (topics) {
		var topicsUser = topicOptions.filter((topic) =>
			defaultOptions(topic, topics)
		);
	}
	return (
		<div className='ProfileForm'>
			<form className='ProfileForm__form' onSubmit={handleSubmit}>
				<p className='ProfileForm__form-title'>Edit your profile</p>
				<div className='ProfileForm__form-img'>
					<img
						className='ProfileForm__form-img-avatar'
						src={
							avatar
								? `${API}/${id}/${category_image}/${avatar}.jpg`
								: 'https://villageinsurancedirect.com/wp-content/uploads/2016/06/default_avatar_community.png'
						}
						alt='User Avatar'
					/>
					<input type='file' onChange={handleChangeImage} />
					<button
						className='ProfileForm__form-img-btn'
						onClick={handleSubmitImage}
					>
						Upload
					</button>
				</div>
				<div className='ProfileForm__form-item'>
					<label>Nickname</label>
					<input
						type='text'
						name='username'
						onChange={handleChange}
						value={username}
					/>
				</div>
				<div className='ProfileForm__form-item'>
					<label>User Bio</label>
					<CKEditor
						config={config}
						data={biography}
						onChange={handleEditor}
						editor={ClassicEditor}
					/>
				</div>
				<div className='ProfileForm__form-item'>
					<label>Email</label>
					<input
						type='email'
						name='email'
						onChange={handleChange}
						value={email}
					/>
				</div>
				<div className='ProfileForm__form-item'>
					<label>Phone Number</label>
					<input
						type='tel'
						name='phone'
						onChange={handleChange}
						value={phone}
					/>
				</div>
				<div className='ProfileForm__form-item'>
					<label>PayPal Account</label>
					<input
						type='text'
						name='paypal_account'
						onChange={handleChange}
						value={paypal_account}
					/>
				</div>

				{user ? (
					user.roles.includes('ROLE_SPONSOR') ? (
						<div>
							<div className='ProfileForm__form-item'>
								<label>Categories</label>

								<Select
									onChange={handleChangeCategories}
									options={category}
									value={categoriesUser}
									isMulti
								/>
							</div>

							<div className='ProfileForm__form-item'>
								<label>Topics</label>

								<Select
									onChange={handleChangeTopics}
									options={topicOptions}
									value={topicsUser}
									isMulti
								/>
							</div>
						</div>
					) : null
				) : null}

				<button className='ProfileForm__form-submit' type='submit'>
					Save
				</button>
			</form>
		</div>
	);
};

export default UserProfileForm;
