import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import ReactHtmlParser from 'react-html-parser';
import {
	deleteEventAction,
	getEventEditAction,
} from '../actions/eventsActions';
import Swal from 'sweetalert2';
import '../assets/styles/components/EventCardUser.scss';
const API = '../eventmaster-backend/resources/assets';
// const API = 'http://eventmaster.site/eventmaster-backend/resources/assets';

const EventCardUser = ({ event, sponsor }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	let {
		id,
		title,
		dateStart,
		location,
		description,
		userId,
		path_image,
		category_image,
		image,
	} = event;

	if (image === '07914edf-0c99-4e46-b3d9-f6a349f1acdd') {
		userId = 1;
		path_image = '07914edf-0c99-4e46-b3d9-f6a349f1acdd.jpg';
		category_image = 'event';
	}
	const redirectEventEdit = (event) => {
		dispatch(getEventEditAction(event));
		history.push(`organizer/eventedit/${event.id}`);
	};
	const deleteEventConfirm = (id) => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'your event will be permanently deleted!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete my event!',
		}).then((result) => {
			if (result.value) {
				dispatch(deleteEventAction(id));
			}
		});
	};
	const dateStartSplit = dateStart.split('T', 1);
	return (
		<>
			<section className='EventCardUser'>
				<div className='EventCardUser__img'>
					<img
						className='EventCardUser__img-item'
						src={`${API}/${userId}/${category_image}/${path_image}`}
						alt='EventImage'
					/>
				</div>
				<div className='EventCardUser__data'>
					<p className='EventCardUser__data-title'>{title}</p>
					<p className='EventCardUser__data-datePlace'>
						{dateStartSplit} / {location}
					</p>
					<p className='EventCardUser__data-description'>
						{ReactHtmlParser(description)}
					</p>
				</div>
				{!sponsor ? (
					<div className='EventCardUser__select'>
						<button
							type='button'
							onClick={() => deleteEventConfirm(id)}
							className='EventCardUser__select-delete'
						>
							Delete
						</button>

						<button
							type='button'
							onClick={() => redirectEventEdit(event)}
							className='EventCardUser__select-edit'
						>
							Edit
						</button>
					</div>
				) : null}
			</section>
		</>
	);
};

export default EventCardUser;
