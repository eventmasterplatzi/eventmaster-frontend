import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import AuthService from "../services/auth.service";
import "../assets/styles/components/NavbarMobile.scss";
import {
  AiFillHome,
  AiOutlineSearch,
  AiOutlineBell,
  AiOutlineUser,
  AiOutlineLogout,
} from "react-icons/ai";

const NavbarMobile = () => {
  const history = useHistory();
  const [user, setUser] = useState({
    showOrganizerBoard: false,
    showSponsorBoard: false,
    currentUser: undefined,
  });
  const handleLogout = () => {
    AuthService.logout();
    history.push("/login");
  };
  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setUser({
        currentUser: user,
        showOrganizerBoard: user.roles.includes("ROLE_ORGANIZER"),
        showSponsorBoard: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }, []);
  const { currentUser, showOrganizerBoard, showSponsorBoard } = user;
  return (
    <div className="NavbarMobile">
      {currentUser ? (
        <div className="NavbarMobile__options">
          <Link to={showOrganizerBoard ? "/organizer" : "/sponsor"}>
            <div className="NavbarMobile__options-item">
              <AiFillHome />
            </div>
          </Link>
          <Link to="/">
            <div className="NavbarMobile__options-item">
              <AiOutlineSearch />
            </div>
          </Link>
          {/* <Link to="/">
            <div className="NavbarMobile__options-item">
              <AiOutlineBell />
            </div>
          </Link> */}
          <Link to="/profile">
            <div className="NavbarMobile__options-item">
              <AiOutlineUser />
            </div>
          </Link>
          <Link to="/login" onClick={handleLogout}>
            <div className="NavbarMobile__options-item">
              <AiOutlineLogout />
            </div>
          </Link>
        </div>
      ) : (
        <div className="NavbarMobile__options">
          <Link to="/login">
            <div className="NavbarMobile__options-text">Log In</div>
          </Link>
          <Link to="/register">
            <div className="NavbarMobile__options-text">Register</div>
          </Link>
        </div>
      )}
    </div>
  );
};

export default NavbarMobile;
