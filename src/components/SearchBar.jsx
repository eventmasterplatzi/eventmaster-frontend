import React from 'react';
import '../assets/styles/components/SearchBar.scss';

const SearchBar = () => {
	return (
		<>
			<section className='SearchBar'>
				<div className='SearchBar__container'>
					<span className='SearchBar__container-icon'> </span>
					<input className='SearchBar__container-input' type='text' />
				</div>
			</section>
		</>
	);
};

export default SearchBar;
