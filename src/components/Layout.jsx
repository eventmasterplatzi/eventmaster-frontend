import React from "react";
import NavBarDesktop from "./NavBarDesktop";
import Footer from "./Footer";
import NavbarMobile from "./NavbarMobile";

const Layout = ({ children }) => {
  return (
    <>
      <NavBarDesktop />
      {children}
      <Footer />
      <NavbarMobile />
    </>
  );
};

export default Layout;
