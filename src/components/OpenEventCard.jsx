import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getEventsAction } from '../actions/eventsActions';
import { getCategoriesAction } from '../actions/categoriesActions';
import { getTopicsAction } from '../actions/topicsActions';
import { getModelsAction } from '../actions/modelsActions';
import CardSponsorMode from '../components/CardSponsorMode';
import ReactHtmlParser from 'react-html-parser';
const API = '../eventmaster-backend/resources/assets';
// const API = 'http://eventmaster.site/eventmaster-backend/resources/assets';
import {
	AiOutlineTags,
	AiTwotoneCalendar,
	AiTwotoneEnvironment,
	AiOutlineClockCircle,
	AiOutlineTeam,
} from 'react-icons/ai';
import '../assets/styles/components/OpenEventCard.scss';

const OpenEventCard = (props) => {
	const dispatch = useDispatch();
	const [currentCategory, setCategory] = useState();
	const [currentTopics, setTopics] = useState();
	const [currentModels, setModels] = useState();
	const [currentEvent, setEvent] = useState();
	const event = useSelector((state) => state.events.events);
	const category = useSelector((state) => state.categories.categories);
	const topics = useSelector((state) => state.topics.topics);
	const models = useSelector((state) => state.models.models);
	useEffect(() => {
		const getCategories = (id) => dispatch(getCategoriesAction(id));
		const getModels = (id) => dispatch(getModelsAction(id));
		const loadEvent = () => dispatch(getEventsAction());
		const loadTopics = () => dispatch(getTopicsAction());
		if (!event.length) {
			loadEvent();
		}
		if (currentEvent) {
			if (!category.length) {
				getCategories(currentEvent.userId);
			}
			// if (!models.length) {
			getModels(currentEvent.userId);
			// }
			if (!topics.length) {
				loadTopics();
			}

			const getEventCategory = category.filter(
				(category) => category.id == currentEvent.categories
			);
			const getEventsTopics = topics.filter((topic) =>
				currents(topic, currentEvent.topics)
			);
			const getEventsModels = models.filter((model) =>
				currents(model, currentEvent.modes)
			);
			setModels(getEventsModels);
			setTopics(getEventsTopics);
			setCategory(getEventCategory[0]);
		}
	}, [currentEvent, category, topics, models]);
	useEffect(() => {
		if (event.length) {
			const getCurrentEvent = event.filter(
				(event) => event.id == props.match.params.eventId
			);
			setEvent(getCurrentEvent[0]);
		}
	}, [event]);
	if (currentEvent) {
		var {
			title,
			dateStart,
			description,
			location,
			event_time,
			userId,
			category_image,
			path_image,
			attendees,
		} = currentEvent;
		if (path_image === '07914edf-0c99-4e46-b3d9-f6a349f1acdd.jpg') {
			userId = 1;
		}
	}
	const currents = (origin, option) => {
		for (let i = 0; i < option.length; i++) {
			if (origin.id == option[i]) {
				return origin;
			}
		}
	};
	return (
		<>
			<main>
				<div className='OpenEventCard'>
					<figure className='OpenEventCard__img'>
						<img
							src={`${API}/${userId}/${category_image}/${path_image}`}
							alt='EventImage'
						/>
					</figure>
					<div className='OpenEventCard__info'>
						<h1 className='OpenEventCard__info-title'>{title}</h1>
						<h3 className='OpenEventCard__info-category'>
							{/* <span className="icon-tags icons"></span> */}
							<AiOutlineTags />
							{currentCategory
								? currentCategory.title
								: null} :{' '}
							{currentCategory
								? currentCategory.description
								: null}
						</h3>
						{currentTopics
							? currentTopics.map((topic) => (
									<p
										key={topic.id}
										className='OpenEventCard__info-topic'
									>
										{topic.title}: {topic.description}
									</p>
							  ))
							: null}
						<p className='OpenEventCard__info-item'>
							{/* <span className="icon-calender icons"></span> */}
							<AiTwotoneCalendar />
							{dateStart ? dateStart.split('T', 1) : null}
						</p>
						<p className='OpenEventCard__info-item'>
							{/* <span className="icon-location icons"></span> */}
							<AiTwotoneEnvironment />
							{location}
						</p>
						<p className='OpenEventCard__info-item'>
							{/* <span className="icon-watch icons"></span> */}
							<AiOutlineClockCircle />
							{event_time}: hrs
						</p>
						<p className='OpenEventCard__info-item'>
							{/* <span className="icon-group icons"></span> */}
							<AiOutlineTeam />
							{attendees} Attendees
						</p>
					</div>
					<div className='OpenEventCard__info-details'>
						<label className='OpenEventCard__details-title'>
							Details
						</label>
						<p className='OpenEventCard__text'>
							{ReactHtmlParser(description)}
						</p>
					</div>
				</div>
				<label className='OpenEventCard__levels-title'>
					Sponsorship Levels
				</label>
				<div className='OpenEventCard__levels'>
					{currentModels
						? currentModels.map((model) => (
								<div
									key={model.id}
									className='OpenEventCard__levels-models'
								>
									<CardSponsorMode
										key={model.id}
										model={model}
										buy={true}
									/>
								</div>
						  ))
						: null}
				</div>
			</main>
		</>
	);
};

export default OpenEventCard;
