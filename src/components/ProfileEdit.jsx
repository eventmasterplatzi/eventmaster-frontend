import React from "react";
import ReactDOM from "react-dom";

import "../assets/styles/components/ProfileEdit.scss";

const ProfileEdit = ({ isShowing, hide, userData }) =>
  isShowing
    ? ReactDOM.createPortal(
        <>
          <div className="ProfileEdit" />
          <div
            className="ProfileEdit__modal"
            aria-modal
            aria-hidden
            tabIndex={-1}
            role="dialog"
          >
            <div className="ProfileEdit__modal-container">
              <div className="ProfileEdit__modal-container-header">
                <button
                  type="button"
                  className="ProfileEdit__modal-container-header-close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={hide}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form className="ProfileEdit__form">
                <p className="ProfileEdit__form-title">Edit your profile</p>
                <div className="ProfileEdit__form-item-img">
                  <label>Image</label>
                  <input type="file" accept="image/*" />
                </div>
                <div className="ProfileEdit__form-item">
                  <label>Fullname</label>
                  <input type="text" defaultValue={userData.name} />
                </div>
                <div className="ProfileEdit__form-item">
                  <label>User Bio</label>
                  <textarea
                    cols="30"
                    rows="5"
                    defaultValue={userData.bio}
                  ></textarea>
                </div>
                <div className="ProfileEdit__form-item">
                  <label>Email</label>
                  <input type="email" defaultValue={userData.email} />
                </div>
                <div className="ProfileEdit__form-item">
                  <label>Phone Number</label>
                  <input type="tel" defaultValue={userData.phone} />
                </div>
                <div className="ProfileEdit__form-item">
                  <label>Country</label>
                  <input type="text" defaultValue={userData.country} />
                </div>
                <div className="ProfileEdit__form-item">
                  <label>City</label>
                  <input type="text" defaultValue={userData.city} />
                </div>
                <button className="ProfileEdit__form-submit">Save</button>
              </form>
            </div>
          </div>
        </>,
        document.body
      )
    : null;

export default ProfileEdit;
