import React, { useState, useEffect } from 'react';
import axiosClient from '../config/axios';
import { useDispatch, useSelector } from 'react-redux';
import Select from 'react-select';
import { useHistory } from 'react-router-dom';
import { getTopicsAction } from '../actions/topicsActions';
import { getCategoriesAction } from '../actions/categoriesActions';
import { getModelsAction } from '../actions/modelsActions';
import { createEventAction } from '../actions/eventsActions';
import '../assets/styles/components/NewEventForm.scss';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import AuthService from '../services/auth.service';
import Swal from 'sweetalert2';
const NewEventForm = () => {
	const config = {
		toolbar: [
			'heading',
			'bold',
			'italic',
			'numberedList',
			'bulletedList',
			'blockQuote',
		],
	};
	const dispatch = useDispatch();
	const history = useHistory();
	const addEvent = (event) => dispatch(createEventAction(event));
	const [event, setEvent] = useState({
		title: '',
		description: '',
		location: '',
		dateStart: '',
		event_time: '',
		attendees: 0,
		categories: [0],
		topics: [0],
		modes: [0],
	});
	const {
		title,
		description,
		location,
		dateStart,
		event_time,
		attendees,
	} = event;
	const [image, setImage] = useState({
		userId: undefined,
		category: '',
	});
	useEffect(() => {
		const user = AuthService.getCurrentUser();
		const loadTopics = () => dispatch(getTopicsAction());
		loadTopics();
		if (user) {
			const loadCategories = (id) => dispatch(getCategoriesAction(id));
			const loadModels = (id) => dispatch(getModelsAction(id));
			loadCategories(user.id);
			loadModels(user.id);
			setImage({
				...image,
				userId: user.id,
				category: 'event',
			});
			event.userId = user.id;
		}
	}, []);
	const loadtopics = useSelector((state) => state.topics.topics);
	const loadcategories = useSelector((state) => state.categories.categories);
	const loadmodels = useSelector((state) => state.models.models);
	const onChangeImage = (e) => {
		image.image = e.target.files[0];
	};
	const onSubmitImage = async (e) => {
		e.preventDefault();
		const fd = new FormData();
		fd.append('file', image.image);
		fd.append('userId', image.userId);
		fd.append('category', image.category);
		await axiosClient.post('images/upload', fd).then((res) => {
			image.id = res.data.id;
			event.image = image.id;
			Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Your photo has been uploaded',
				showConfirmButton: true,
			});
		});
	};
	const onChangeEvent = (e) => {
		setEvent({
			...event,
			[e.target.name]: e.target.value,
		});
	};
	const onChangeEditor = (e, editor) => {
		setEvent({
			...event,
			description: editor.getData(),
		});
	};
	const onChangeAttendees = (e) => {
		setEvent({
			...event,
			attendees: Number(e.target.value),
		});
	};
	const onChangeEventSelect = (e) => {
		setEvent({
			...event,
			categories: [e.value],
		});
	};
	const onChangeEventSelectTopics = (e) => {
		setEvent({
			...event,
			topics: e.map((value) => value.value),
		});
	};
	const onChangeEventSelectModes = (e) => {
		setEvent({
			...event,
			modes: e.map((value) => value.value),
		});
	};

	const topicOptions = loadtopics.map((topic) => {
		return { value: topic.id, label: topic.title };
	});
	const levelOptions = loadmodels.map((model) => {
		return { value: model.id, label: model.title };
	});
	const category = loadcategories.map((category) => {
		return { value: category.id, label: category.title };
	});
	const onSubmitEvent = (e) => {
		e.preventDefault();
		addEvent(event);
		console.log(event);
		history.push('/organizer');
	};
	return (
		<>
			<div className='NewEventForm'>
				<h1 className='NewEventForm__title'>Create Event</h1>
				<form className='NewEventForm__form' onSubmit={onSubmitEvent}>
					<div className='NewEventForm__form-item img-container'>
						<label>Image</label>
						<input type='file' onChange={onChangeImage} />
						<button onClick={onSubmitImage}>Upload</button>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Title</label>
						<input
							name='title'
							value={title}
							onChange={onChangeEvent}
							type='text'
							placeholder='Title'
						/>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Place</label>
						<input
							name='location'
							value={location}
							onChange={onChangeEvent}
							type='text'
							placeholder='Place'
						/>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Date</label>
						<input
							name='dateStart'
							onChange={onChangeEvent}
							value={dateStart}
							type='date'
						/>
					</div>
					<div className='NewEventForm__form-item'>
						<label>Time</label>
						<input
							name='event_time'
							value={event_time}
							onChange={onChangeEvent}
							type='time'
						/>
					</div>
					<div className='NewEventForm__form-item description-newevent'>
						<label>Description</label>
						{/* <textarea
							name='description'
							value={description}
							onChange={onChangeEvent}
							cols='10'
							rows='5'
						></textarea> */}
						<CKEditor
							config={config}
							onChange={onChangeEditor}
							editor={ClassicEditor}
						/>
					</div>
					<div className='NewEventForm__form-item attendees-newevent'>
						<label>Attendees </label>
						<input
							name='attendees'
							value={attendees}
							onChange={onChangeAttendees}
							type='number'
						/>
					</div>
					<div className='NewEventForm__form-item category-newevent'>
						<label>Category </label>
						<Select
							onChange={onChangeEventSelect}
							options={category}
						/>
					</div>
					<div className='NewEventForm__form-item topics-newevent'>
						<label>Topics</label>
						<Select
							onChange={onChangeEventSelectTopics}
							options={topicOptions}
							isMulti
						/>
					</div>
					<div className='NewEventForm__form-item levels-newevent'>
						<label>Sponsorship Levels</label>
						<Select
							onChange={onChangeEventSelectModes}
							options={levelOptions}
							isMulti
						/>
					</div>
					<button className='NewEventForm__form-submit' type='submit'>
						Save
					</button>
				</form>
			</div>
		</>
	);
};
export default NewEventForm;
