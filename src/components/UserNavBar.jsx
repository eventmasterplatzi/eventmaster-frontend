import React from "react";
import "../assets/styles/components/UserNavBar.scss";

const UserNavBar = () => {
  return (
    <div className="UserNavBar">
      <button className="UserNavBar__option">Sponsored</button>
      <button className="UserNavBar__option">On Approval</button>
      <button className="UserNavBar__option">More Events</button>
    </div>
  );
};

export default UserNavBar;
