import React from 'react';
import { Link } from 'react-router-dom';
import '../assets/styles/components/EventCard.scss';
const API = '../eventmaster-backend/resources/assets';
// const API = "http://eventmaster.site/eventmaster-backend/resources/assets";

const EventCard = ({ event }) => {
	let {
		id,
		title,
		dateStart,
		location,
		userId,
		path_image,
		category_image,
		image,
	} = event;
	if (image === '07914edf-0c99-4e46-b3d9-f6a349f1acdd') {
		userId = 1;
		path_image = '07914edf-0c99-4e46-b3d9-f6a349f1acdd.jpg';
		category_image = 'event';
	}
	const dateStartSplit = dateStart.split('T', 1);
	return (
		<>
			<Link to={`/event/${id}`}>
				<article className='EventCard'>
					<img
						className='EventCard__img'
						src={`${API}/${userId}/${category_image}/${path_image}`}
						alt='EventImage'
					/>
					<div className='EventCard__data'>
						<p className='EventCard__data-title'>{title}</p>
						<p className='EventCard__data-date'>
							Date: {dateStartSplit}
						</p>
						<p className='EventCard__data-user'>
							Place: {location}
						</p>
					</div>
				</article>
			</Link>
		</>
	);
};

export default EventCard;
