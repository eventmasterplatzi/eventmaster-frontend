import React, { useState, useRef, useEffect } from "react";
import "../assets/styles/components/Paypal.scss";

function Product({ model }) {
  const { title, ammount } = model;
  const [paidFor, setPaidFor] = useState(false);
  const [error, setError] = useState(null);
  const paypalRef = useRef();

  useEffect(() => {
    window.paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: title,
                amount: {
                  currency_code: "USD",
                  value: Number(ammount),
                },
              },
            ],
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          setPaidFor(true);
          console.log(order);
        },
        onError: (err) => {
          setError(err);
          console.error(err);
        },
      })
      .render(paypalRef.current);
  }, [title, Number(ammount)]);

  if (paidFor) {
    return (
      <div>
        <h1>Congrats, you just bought {title}!</h1>
      </div>
    );
  }

  return (
    <div>
      {error && <div>Uh oh, an error occurred! {error.message}</div>}
      <h1>
        {title} for ${ammount}
      </h1>
      <div ref={paypalRef} />
    </div>
  );
}

export default Product;
