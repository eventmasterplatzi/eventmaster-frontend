import React from "react";
import "../assets/styles/components/Categories.scss";

const Categories = ({ children, title }) => (
  <div className="Categories">
    <h4 className="Categories__title">{title}</h4>
    {children}
  </div>
);

export default Categories;
