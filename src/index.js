import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/Globals.scss';
import { Provider } from 'react-redux';
import App from './routes/App';
import store from './store';
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);
