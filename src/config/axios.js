import axios from 'axios';

const axiosClient = axios.create({
    baseURL: 'http://eventmaster.site:3002/api/v1/',
});

export default axiosClient;
