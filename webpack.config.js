const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const TerserJSPlugin = require("terser-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
    entry: {
        app: path.resolve(__dirname, "src/index.js"),
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "js/[name].[hash].js",
        //   publicPath: "dist/",
        chunkFilename: "js/[id].[chunkhash].js",
    },
    optimization: {
        minimizer: [new TerserJSPlugin(), new OptimizeCSSAssetsPlugin()],
    },
    resolve: {
        extensions: [".js", ".jsx"],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                    },
                ],
            },
            {
                test: /\.(s*)css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader",
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|gif|jpg|mp4|web|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[hash].[ext]",
                            limit: 1000,
                            outputPath: "assets/images/",
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            filename: "assets/styles/[name].[hash].css",
            chunkFilename: "assets/styles/[name].[hash].css",
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ["**/app.*"],
        }),
        new HtmlWebPackPlugin({
            template: "./public/index.html",
            filename: "./index.html",
        }),
        new AddAssetHtmlPlugin({
            filepath: path.resolve(__dirname, "dist/js/*.dll.js"),
            outputPath: "js",
            publicPath: "dist/js",
        }),
        new webpack.DllReferencePlugin({
            manifest: require("./modules-manifest.json"),
            context: path.resolve(__dirname, "src"),
        }),
    ],
    optimization: {
        splitChunks: {
            chunks: "all",
            minSize: 0,
            name: "commons",
        },
    },
};
