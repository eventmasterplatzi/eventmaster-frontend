# Event Master

This is a marketplace application meant to connect companies and event organizers looking for sponsorship.

## Implemented technologies

- React.js
- Redux
- Axios
- SCSS
- Webpack

## Features

- User profile
- User dashboard
- Event explorer
- Event chat (Organizer - Sponsor)
- PayPal payment gateway

You can also:

- Customize you events.
- Create as many sponsorship levels as possible for every event

### Installation

EventMaster requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies.

```sh
$ cd EventMaster-Frontend
$ npm install
```

### Development

Want to contribute? Great!

Open your favorite Terminal and run these commands.

#### Building for source

Generating pre-built archives for deploy:

```sh
$ npm run build:dll
```

For production release:

```sh
$ npm run build
```

#### Development mode

```sh
$ npm run start
```

Yu can see our first FrontEnd documentation and designs in notion here:

```bash
   https://www.notion.so/Frontend-EventMaster-resoucers-a8f4ab25ec64426d8542853968da75a4
```

### MuckUp Figma
Muckup [Figma](https://www.figma.com/proto/5z2MjsmECSZuYmwjUjQWDx/EventMaster?node-id=81%3A118&scaling=min-zoom)

## License

MIT

**Support public software!**
